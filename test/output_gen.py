# top=lib::output_gen

from spade import *


# Ensures that when sending `bit` of `color`, the result is a zero waveform
async def ensure_zero(s, color, bit):
    s.i.control = f"OutputControl::Led({color}, {bit}, 0)"
    await Timer(1, units="ps")
    s.o.assert_eq("true")

    s.i.control = f"OutputControl::Led({color}, {bit}, 40)"
    await Timer(1, units="ps")
    s.o.assert_eq("true")

    s.i.control = f"OutputControl::Led({color}, {bit}, 41)"
    await Timer(1, units="ps")
    s.o.assert_eq("false")


# Ensures that when sending `bit` of `color`, the result is a one waveform
async def ensure_one(s, color, bit):
    s.i.control = f"OutputControl::Led({color}, {bit}, 0)"
    await Timer(1, units="ps")
    s.o.assert_eq("true")

    s.i.control = f"OutputControl::Led({color}, {bit}, 80)"
    await Timer(1, units="ps")
    s.o.assert_eq("true")

    s.i.control = f"OutputControl::Led({color}, {bit}, 81)"
    await Timer(1, units="ps")
    s.o.assert_eq("false")


@cocotb.test()
async def ret_works(dut):
    s = SpadeExt(dut)

    s.i.t = """Timing$(
        us280: 2800,
        us0_4: 40,
        us0_8: 80,
        us0_45: 45,
        us0_85: 85,
        us1_25: 125,
    )"""

    s.i.control = "OutputControl::Ret()"
    await Timer(1, units="ps")
    s.o.assert_eq("false")


@cocotb.test()
async def one_at_bit_0(dut):
    s = SpadeExt(dut)

    s.i.t = """Timing$(
        us280: 2800,
        us0_4: 40,
        us0_8: 80,
        us0_45: 45,
        us0_85: 85,
        us1_25: 125,
    )"""

    # First bit works
    await ensure_one(s, "Color$(g: 0b1000_0000u, r: 0, b: 0)", 0)
    await ensure_zero(s, "Color$(g: 0b0111_1111u, r: 0xffu, b: 0xffu)", 0)

    # Bit 7 works
    await ensure_one(s, "Color$(g: 0b0000_0001u, r: 0, b: 0)", 7)
    await ensure_zero(s, "Color$(g: 0b1111_1110u, r: 0xffu, b: 0xffu)", 7)

    # Bit 8 works
    await ensure_one(s, "Color$(g: 0, r: 0b1000_0000u, b: 0)", 8)
    await ensure_zero(s, "Color$(g: 0xffu, r: 0b0111_1111u, b: 0xffu)", 8)

    # Bit 15 works
    await ensure_one(s, "Color$(g: 0, r: 0b0000_0001u, b: 0)", 15)
    await ensure_zero(s, "Color$(g: 0xffu, r: 0b1111_1110u, b: 0xffu)", 15)

    # Bit 16 works
    await ensure_one(s, "Color$(g: 0, r: 0, b: 0b1000_0000u)", 16)
    await ensure_zero(s, "Color$(g: 0xffu, r: 0xffu, b: 0b0111_1111u)", 16)

    # Bit 23 works
    await ensure_one(s, "Color$(g: 0, r: 0, b: 0b0000_0001u)", 23)
    await ensure_zero(s, "Color$(g: 0xffu, r: 0xffu, b: 0b1111_1110u)", 23)
